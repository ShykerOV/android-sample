package com.movies.app.fragment.profile.fragments.followers.vm

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.movies.app.aac.BaseViewModel
import com.movies.app.base.*
import com.movies.app.fragment.profile.fragments.followers.data.FollowersRepository
import com.movies.app.fragment.profile.fragments.followers.data.ProfileFollowersDataSourceFactory
import com.movies.app.network.RatersAdmin
import com.movies.app.network.responses.FollowResponse
import com.movies.app.network.responses.RatersUser
import com.movies.app.prefs.RatersPreference

class FollowersViewModel(
    api: RatersAdmin,
    sp: RatersPreference,
    app: Application,
    private val repo: FollowersRepository,
    id: Long?
) : BaseViewModel(app) {


    private val _followState: MutableLiveData<ViewState<Pair<Long, FollowResponse>>> =
        MutableLiveData()

    val followState: LiveData<ViewState<Pair<Long, FollowResponse>>>
        get() = _followState

    val zeroPost = MutableLiveData<Boolean?>(null)

    private val _followStateSearch: MutableLiveData<ViewState<Pair<Long, FollowResponse>>> =
        MutableLiveData()

    val followStateSearch: LiveData<ViewState<Pair<Long, FollowResponse>>>
        get() = _followStateSearch

    val query: MutableLiveData<String> = MutableLiveData()

    val searchState: LiveData<PagedList<RatersUser>>

    val followersState: LiveData<PagedList<RatersUser>>


    private val pagingDs = ProfileFollowersDataSourceFactory(api, sp)
    private val searchDs = ProfileFollowersDataSourceFactory(api, sp)


    init {
        searchState = LivePagedListBuilder<Int, RatersUser>(
            searchDs,
            basePagedListConfig
        ).setFetchExecutor(executor)
            .build()
        pagingDs.authorId = id
        followersState = LivePagedListBuilder<Int, RatersUser>(
            pagingDs,
            basePagedListConfig
        ).setBoundaryCallback(object : PagedList.BoundaryCallback<RatersUser>() {
            override fun onZeroItemsLoaded() {
                super.onZeroItemsLoaded()
                if(zeroPost.value == null) {
                    zeroPost.postValue(true)
                }
            }

        }).setFetchExecutor(executor)
            .build()

        query.value = ""
    }


    fun getNewSearchList(name: String, authorId: Long? = null) {
        if (searchDs.name != name) {
            searchDs.authorId = authorId
            searchDs.name = name
            searchState.value?.dataSource?.invalidate()
        }
    }


    fun onFollowClick(
        friend: RatersUser,
        userId: Long,
        fromSearch: Boolean
    ) {
        if(!fromSearch) {
            if (friend.is_following) {
                unfollow(friend, userId)
            } else {
                follow(friend, userId)
            }
        }else{
            if (friend.is_following) {
                unfollowSearch(friend, userId)
            } else {
                followSearch(friend, userId)
            }
        }
    }

    private fun follow(friend: RatersUser, position: Long) {
        disposables.add(
            repo.follow(friend, position)
                .map { it.toViewState() }
                .startWith(ViewState.loading())
                .subscribe({ _followState.postValue(it) },
                    { FirebaseCrashlytics.getInstance().recordException(it) }
                )

        )
    }

    private fun unfollow(friend: RatersUser, position: Long) {
        disposables.add(
            repo.unfollow(friend, position)
                .map { it.toViewState() }
                .startWith(ViewState.loading())
                .subscribe({ _followState.postValue(it) },
                    { FirebaseCrashlytics.getInstance().recordException(it) }
                )
        )
    }

    private fun followSearch(friend: RatersUser, position: Long) {
        disposables.add(
            repo.follow(friend, position)
                .map { it.toViewState() }
                .startWith(ViewState.loading())
                .subscribe({ _followStateSearch.postValue(it) },
                    { FirebaseCrashlytics.getInstance().recordException(it) }
                )

        )
    }

    private fun unfollowSearch(friend: RatersUser, position: Long) {
        disposables.add(
            repo.unfollow(friend, position)
                .map { it.toViewState() }
                .startWith(ViewState.loading())
                .subscribe({ _followStateSearch.postValue(it) },
                    { FirebaseCrashlytics.getInstance().recordException(it) }
                )
        )
    }


}