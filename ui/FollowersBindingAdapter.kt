package com.movies.app.fragment.profile.fragments.followers.ui

import androidx.databinding.BindingAdapter
import androidx.paging.PagedList
import androidx.recyclerview.widget.RecyclerView
import com.movies.app.base.ViewState
import com.movies.app.base.results
import com.movies.app.fragment.friends.FriendsAdapter
import com.movies.app.fragment.friends.OnFriendSelectedListener
import com.movies.app.fragment.friends.OnUserSelected
import com.movies.app.fragment.likes.LikesAdapter
import com.movies.app.fragment.likes.OnFollowSearch
import com.movies.app.fragment.likes.OnUserClick
import com.movies.app.fragment.search.SearchUserAdapter
import com.movies.app.network.responses.FollowResponse
import com.movies.app.network.responses.RatersUser

@BindingAdapter(
    "adapter",
    "followers_state",
    "follow_state",
    "on_follow_click",
    "on_user_click",
    "currentUserId",
    requireAll = false
)
fun RecyclerView.bindFollow(
    inputAdapter: LikesAdapter,
    data: ViewState<List<RatersUser>>?,
    followState: ViewState<Pair<Long, FollowResponse>>?,
    onFollowClick: OnFollowSearch,
    onUserClick: OnUserClick,
    currentUserId: Long
) {
    if (adapter == null) {
        inputAdapter.onFollowSearch = onFollowClick
        inputAdapter.currentUserId = currentUserId
        inputAdapter.onUserClick = onUserClick
        adapter = inputAdapter
    }
    data?.let {
        it.results?.let {
            bindResult(it)
        }
    }
    followState?.let {
        followState.results?.let {
            bindFollowState(it)
        }
    }
}


private fun RecyclerView.bindResult(users: List<RatersUser>): Unit =
    with(adapter as LikesAdapter) {
        setList(users.toMutableList())
    }


private fun RecyclerView.bindFollowState(state: Pair<Long, FollowResponse>): Unit =
    with(adapter as LikesAdapter) {
        commitFollowChanges(state.first, state.second.is_following)
    }



@BindingAdapter(
    "adapter",
    "new_followers",
    "new_follow_state",
    "new_on_follow_click",
    "new_on_user_click",
    "currentUserId",

    requireAll = false)
fun RecyclerView.newBindFollowers(
    inputAdapter: FriendsAdapter,
    data: PagedList<RatersUser>?,
    followState: ViewState<Pair<Long, FollowResponse>>?,
    onFollow:OnFriendSelectedListener,
    onUserSelected: OnUserSelected,
    currentUserId: Long
){
    if(adapter == null){
        inputAdapter.currentUserId = currentUserId
        inputAdapter.onFriendSelectedListener = onFollow
        inputAdapter.onUserSelected = onUserSelected
        adapter = inputAdapter
    }
    data?.let{
        with(adapter as FriendsAdapter){
            submitList(it)
        }
    }
    followState?.let {
        followState.results?.let {
            with(adapter as FriendsAdapter){
                currentList!![it.first.toInt()]?.is_following = it.second.is_following
                notifyItemChanged(it.first.toInt())

            }
        }
    }
}
@BindingAdapter(
    "adapter",
    "new_searchState",
    "new_follow_state",
    "new_on_follow_click",
    "new_on_user_click",
    "currentUserId",
    requireAll = false)
fun RecyclerView.newBindFollowersSearch(
    inputAdapter: FriendsAdapter,
    data: PagedList<RatersUser>?,
    followState: ViewState<Pair<Long, FollowResponse>>?,
    onFollow:OnFriendSelectedListener,
    onUserSelected: OnUserSelected,
    currentUserId: Long

){
    if(adapter == null){
        inputAdapter.currentUserId = currentUserId
        inputAdapter.onFriendSelectedListener = onFollow
        inputAdapter.onUserSelected = onUserSelected
        adapter = inputAdapter
    }
    data?.let{
        with(adapter as FriendsAdapter){
            submitList(it)
        }
    }
    followState?.let {
        followState.results?.let {
            with(adapter as FriendsAdapter){
                currentList!![it.first.toInt()]?.is_following = it.second.is_following
                notifyItemChanged(it.first.toInt())

            }
        }
    }
}

@BindingAdapter(
    "adapter",
    "new_searchState",
    "new_follow_state",
    "new_on_follow_click",
    "new_on_user_click",
    "currentUserId",
    requireAll = false)
fun RecyclerView.newBindFollowersSearch(
    inputAdapter: SearchUserAdapter,
    data: PagedList<RatersUser>?,
    followState: ViewState<Pair<Long, FollowResponse>>?,
    onFollow:OnFriendSelectedListener,
    onUserSelected: OnUserSelected,
    currentUserId: Long

){
    if(adapter == null){
        inputAdapter.currentUserId = currentUserId
        inputAdapter.onFriendSelectedListener = onFollow
        inputAdapter.onUserSelected = onUserSelected
        adapter = inputAdapter
    }
    data?.let{
        with(adapter as SearchUserAdapter){
            submitList(it)
        }
    }
    followState?.let {
        followState.results?.let {
            with(adapter as SearchUserAdapter){
                currentList!![it.first.toInt()]?.is_following = it.second.is_following
                notifyItemChanged(it.first.toInt())

            }
        }
    }
}