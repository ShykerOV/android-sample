package com.movies.app.fragment.profile.fragments.followers.data

import arrow.core.Either
import com.movies.app.fragment.profile.fragments.following.data.FollowinglistDataSource
import com.movies.app.network.responses.FollowResponse
import com.movies.app.network.responses.RatersError
import com.movies.app.network.responses.RatersUser
import com.movies.app.network.responses.toError
import com.movies.app.platform.left
import com.movies.app.platform.right
import com.movies.app.prefs.RatersPreference
import io.reactivex.Flowable

class FollowersRepository(
    private val sp: RatersPreference,
    private val remote: FollowersDataSource
) {

    fun follow(
        friend: RatersUser,
        friendId: Long
    ): Flowable<Either<RatersError, Pair<Long, FollowResponse>>> =
        remote.follow(sp.user?.id!!, friend.id)
            .map { right<RatersError, Pair<Long, FollowResponse>>(Pair(friendId, it)) }
            .onErrorReturn { left(it.toError()) }

    fun unfollow(
        friend: RatersUser,
        friendId: Long
    ): Flowable<Either<RatersError, Pair<Long, FollowResponse>>> =
        remote.unfollow(sp.user?.id!!, friend.id)
            .map { right<RatersError, Pair<Long, FollowResponse>>(Pair(friendId, it)) }
            .onErrorReturn { left(it.toError()) }

}