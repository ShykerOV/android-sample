package com.movies.app.fragment.profile.fragments.followers.data

import androidx.paging.DataSource
import com.movies.app.fragment.profile.fragments.watchlist.data.WatchListDataSource
import com.movies.app.network.RatersAdmin
import com.movies.app.network.responses.FeedResponseData
import com.movies.app.network.responses.RatersUser
import com.movies.app.prefs.RatersPreference

class ProfileFollowersDataSourceFactory(
    val admin: RatersAdmin,
    val sp: RatersPreference
) : DataSource.Factory<Int, RatersUser>() {

    var authorId: Long? = null
    var name: String? = null

    override fun create(): DataSource<Int, RatersUser> {
        return ProfileFollowersDataSource(admin, sp, authorId, name)
    }

}