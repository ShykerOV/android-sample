package com.movies.app.fragment.profile.fragments.followers.ui

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.movies.app.R
import com.movies.app.base.fragment.BaseFragment
import com.movies.app.databinding.FragmentFollowersBinding
import com.movies.app.fragment.friends.FriendsAdapter
import com.movies.app.fragment.likes.LikesAdapter
import com.movies.app.fragment.profile.fragments.followers.vm.FollowersViewModel
import com.movies.app.platform.gone
import com.movies.app.platform.visible
import com.movies.app.prefs.RatersPreference
import com.movies.app.view.AppBar
import kotlinx.android.synthetic.main.fragment_followers.*
import kotlinx.android.synthetic.main.fragment_followers.recycler_list
import kotlinx.android.synthetic.main.fragment_followers.recycler_search
import kotlinx.android.synthetic.main.fragment_followers.search_view
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class FollowersFragment: BaseFragment<FragmentFollowersBinding>() {
    override fun getLayoutId(): Int = R.layout.fragment_followers

    override fun getAppBarTitle(): String = ""

    override fun appBarMode(): AppBar.AppBarEnum = AppBar.AppBarEnum.NONE

    private val viewModel by viewModel<FollowersViewModel> { parametersOf(arguments?.getLong("id", -1)) }

    private val sp by inject<RatersPreference>()

    companion object {
        fun getInstance(id: Long?): FollowersFragment =
            FollowersFragment().apply {
                id?.let {
                    arguments = Bundle().apply { putLong("id", id) }
                }
            }
    }

    override fun onFragmentCreated(view: View, savedInstanceState: Bundle?) {
        super.onFragmentCreated(view, savedInstanceState)
        val id = if (arguments != null) {
            arguments?.getLong("id", -1)
        } else {
            null
        }
        viewBinding.delegate =
            FollowersDelegate(
                viewModel,
                FriendsAdapter(),
                FriendsAdapter(),
                id,
                findNavController()
            )
        viewModel.zeroPost.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                if(sp.user?.id!! != arguments?.getLong("id", -1)){
                    checkout.gone()
                }else{
                    checkout.visible()
                }
                empty_recycler.visible()
                recycler_list.gone()
                search_view.gone()

            } else if (it != null && !it) {
                empty_recycler.gone()
                search_view.visible()
                recycler_list.visible()
            }
        })
        viewModel.followersState.observe(viewLifecycleOwner, Observer {
            if (it.size > 0) {
                viewModel.zeroPost.value = false
            }
        })

        viewBinding.currentUserId = sp.user!!.id!!

        viewModel.query.observe(this, Observer {
            if (it.isNotEmpty()) {
                recycler_search.visible()
                recycler_list.gone()
                viewModel.getNewSearchList(it, id)
            } else {
                recycler_search.gone()
                recycler_list.visible()
            }
        })

    }

    override fun onPause() {
        super.onPause()
    }
}