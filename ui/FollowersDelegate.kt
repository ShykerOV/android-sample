package com.movies.app.fragment.profile.fragments.followers.ui

import android.os.Bundle
import androidx.navigation.NavController
import com.movies.app.R
import com.movies.app.fragment.friends.FriendsAdapter
import com.movies.app.fragment.profile.fragments.followers.vm.FollowersViewModel
import com.movies.app.network.responses.RatersUser

class FollowersDelegate(
    val viewModel: FollowersViewModel,
    val adapter: FriendsAdapter,
    val adapterSearch: FriendsAdapter,
    id: Long?,
    val findNavController: NavController
) {


    fun onQueryChange(new: String): Unit =
        with(viewModel) { query.value = new }

    fun onFollowClick(friend: RatersUser, userId: Long, isFromSearch: Boolean) {
        viewModel.onFollowClick(friend, userId, isFromSearch)
    }

    fun onUserClick(user: RatersUser) {
        findNavController.navigate(
            R.id.action_profileFragment_self,
            Bundle().apply { putParcelable("user", user) })
    }
}