package com.movies.app.fragment.profile.fragments.followers.data

import com.movies.app.followds.FollowDataSource
import com.movies.app.network.RatersAdmin
import com.rmovies.app.network.errors.FeedEnded
import com.movies.app.network.responses.FollowResponse
import com.movies.app.network.responses.RatersUser
import com.movies.app.platform.schedulers.BaseSchedulers
import io.reactivex.Flowable


interface FollowersDataSource : FollowDataSource {
}

class FollowersDataSourceImpl(
    private val api: RatersAdmin,
    private val schedulers: BaseSchedulers
) : FollowersDataSource {

    override fun unfollow(userId: Long, friendId: Long): Flowable<FollowResponse> =
        api.unfollow(userId, friendId)
            .subscribeOn(schedulers.io())
            .map { it.data }


    override fun follow(userId: Long, friendId: Long): Flowable<FollowResponse> =
        api.follow(userId, friendId)
            .subscribeOn(schedulers.io())
            .map { it.data }

}
