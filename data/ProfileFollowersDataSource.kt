package com.movies.app.fragment.profile.fragments.followers.data

import androidx.paging.PageKeyedDataSource
import com.movies.app.network.RatersAdmin
import com.movies.app.network.responses.RatersResponse
import com.movies.app.network.responses.RatersUser
import com.movies.app.prefs.RatersPreference
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileFollowersDataSource(
    private val ratersApi: RatersAdmin,
    private val sp: RatersPreference,
    private val authorId: Long?,
    private val name: String?
) : PageKeyedDataSource<Int, RatersUser>() {
    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, RatersUser>
    ) {
        ratersApi.getUserFollowersList(sp.user?.id!!, authorId, 1, name)
            .enqueue(object : Callback<RatersResponse<List<RatersUser>>> {
                override fun onFailure(
                    call: Call<RatersResponse<List<RatersUser>>>,
                    t: Throwable
                ) {

                }

                override fun onResponse(
                    call: Call<RatersResponse<List<RatersUser>>>,
                    response: Response<RatersResponse<List<RatersUser>>>
                ) {
                    if (response.isSuccessful) {

                        callback.onResult(response.body()!!.data, null, 2)
                    }
                }


            }

            )
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, RatersUser>) {
        ratersApi.getUserFollowersList(sp.user?.id!!, authorId, params.key, name)
            .enqueue(object : Callback<RatersResponse<List<RatersUser>>> {
                override fun onFailure(
                    call: Call<RatersResponse<List<RatersUser>>>,
                    t: Throwable
                ) {
                }

                override fun onResponse(
                    call: Call<RatersResponse<List<RatersUser>>>,
                    response: Response<RatersResponse<List<RatersUser>>>
                ) {
                    if (response.isSuccessful) {
                        val nextKey =
                            if (response.body()!!.data.isEmpty()) null
                            else
                                params.key + 1
                        callback.onResult(response.body()!!.data, nextKey)
                    }
                }


            }

            )
    }

    override fun loadBefore(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, RatersUser>
    ) {
    }
}
